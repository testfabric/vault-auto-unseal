FROM debian:stretch-slim

RUN apt-get update \
  && apt-get install -y dumb-init \
  && apt-get clean

COPY vault-auto-unseal /usr/local/bin/vault-auto-unseal

RUN chmod +x /usr/local/bin/vault-auto-unseal

ENTRYPOINT ["/usr/bin/dumb-init", "--"]