package cmd

import (
	"fmt"
	"github.com/hashicorp/vault/api"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "vault-auto-unseal",
	Short: "Vault auto unseal is a simple tool for unsealing vault instances",
	RunE: unsealVault,
}

func init() {
	// Setup flags
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	viper.SetDefault("addr", "http://localhost:8200")
	viper.SetDefault("skip_verify", false)
	viper.SetEnvPrefix("vault")
	viper.AutomaticEnv()
}

func unsealVault(cmd *cobra.Command, args []string) error {
	config := &api.Config{
		Address: viper.GetString("addr"),
	}

	tlsConfig := &api.TLSConfig{
		Insecure: viper.GetBool("skip_verify"),
	}

	if viper.IsSet("cacert") {
		tlsConfig.CACert = viper.GetString("cacert")
	}

	err := config.ConfigureTLS(tlsConfig)
	if err != nil {
		return err
	}

	client, err := api.NewClient(config)
	if err != nil {
		return err
	}

	status, err := client.Sys().SealStatus()
	if err != nil {
		return err
	}

	if !viper.IsSet("unseal_key") {
		return fmt.Errorf("required environment variable 'VAULT_UNSEAL_KEY' has not been set")
	}

	if status.Sealed {
		response, err := client.Sys().Unseal(viper.GetString("unseal_key"))
		if err != nil {
			return err
		}

		if response.Sealed {
			return fmt.Errorf("vault instance did not unseal, more than one key?")
		}
	}

	return nil
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}