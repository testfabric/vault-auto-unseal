# Vault Auto Unseal

A simple mechanism for automatically unsealing a Hashicorp Vault instance, intended to be run as a Kubernetes sidecar.

Generally using this is a poor idea, and you should be using some kind of managed system.

## Required Environment Variables

| Variable         | Description                |
|------------------|----------------------------|
| VAULT_UNSEAL_KEY | A base64 encoded key shard |

## Optional Environment Variables

| Variable          | Default               | Description                                        |
|-------------------|-----------------------|----------------------------------------------------|
| VAULT_ADDR        | http://localhost:8200 | The address of the vault server                    |
| VAULT_CACERT      | ""                    | The Certificate Authority to use for verifying TLS |
| VAULT_SKIP_VERIFY | false                 | Skip verification of TLS certificates (DO NOT USE) |

## Running

```
vault-auto-unseal
```